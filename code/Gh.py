#!/usr/bin/env python
# encoding: utf-8

"""
@version: 0.1
@author: zCoding
@time: 2018/4/28 23:05 Python3
@describe: 找出所有的干旱过程
"""
import pandas as pd
import matplotlib.pyplot as plt
import os
import warnings

warnings.filterwarnings("ignore")


def ganhanguocheng(path):
    data = pd.read_csv(path, sep=',', encoding='GB2312')

    nianfen = list(set(data["年"]))

    ghindex = -0.5
    siji = [5, 8, 11, 14]
    sijis = {5: '春', 8: '夏', 11: '秋', 14: '冬'}
    GHsj = pd.DataFrame(columns=['区站号', '年', '季节', '天数', '轻度', '中度', '严重'])
    for i in nianfen:
        nian = data[data["年"] == i]
        for j in siji:
            yiji = nian[nian.月 <= j]
            nian.drop(nian[nian.月 <= j].index, inplace=True)
            ganhan = 0
            for m in yiji.index:
                if ganhan >= 10:
                    if m == yiji.index[-1]:
                        print('123')
                        temp = yiji.loc[m - ganhan:m]
                        a = temp[temp.MCI <= -1.5].count()['MCI']
                        b = temp[temp.MCI <= -1.0].count()['MCI']
                        c = temp[temp.MCI <= -0.5].count()['MCI']

                        GHsj = GHsj.append(
                            {'区站号': temp.loc[m, '区站号'], '年': temp.loc[m, '年'], '季节': sijis[j], '天数': len(temp),
                             '轻度': c - b, '中度': b - a, '严重': a}, ignore_index=True)
                        ganhan = 0
                        continue
                    if yiji.loc[m:m + 10][yiji.loc[m:m + 10].MCI > ghindex].count()['MCI'] < 10:
                        ganhan = ganhan + 1
                    else:
                        temp = yiji.loc[m - ganhan:m + 10]
                        a = temp[temp.MCI <= -1.5].count()['MCI']
                        b = temp[temp.MCI <= -1.0].count()['MCI']
                        c = temp[temp.MCI <= -0.5].count()['MCI']

                        GHsj = GHsj.append(
                            {'区站号': temp.loc[m, '区站号'], '年': temp.loc[m, '年'], '季节': sijis[j], '天数': len(temp),
                             '轻度': c - b, '中度': b - a, '严重': a}, ignore_index=True)
                        GHsj = GHsj[['区站号', '年', '季节', '天数', '轻度', '中度', '严重']]
                        ganhan = 0
                    continue

                if yiji.loc[m, 'MCI'] < ghindex:
                    ganhan = ganhan + 1
                if yiji.loc[m, 'MCI'] > ghindex and ganhan < 10:
                    ganhan = 0

    # GHsj.to_csv('../data/GHSJ/' + str(path[-9:-4]) + '.csv', sep=',', encoding='GB2312', index=None)


if __name__ == '__main__':
    file_dir = 'D:/zCoding/DaQi/data/afterMCI'
    for root, dirs, files in os.walk(file_dir):
        print("\n当前目录路径", root)  # 当前目录路径
        # print("\n当前路径下所有非目录子文件", files)  # 当前路径下所有非目录子文件

    for fileName in files:
        path = root + "/" + fileName
        ganhanguocheng(path)
        print(path)
    print('finish')
