#!/usr/bin/env python
# encoding: utf-8

"""
@version: 0.1
@author: zCoding
@time: 2018/4/27 10:29 Python3
@describe: ....
"""

import pandas as pd
import matplotlib.pyplot as plt

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.rcParams['axes.unicode_minus'] = False
data = pd.read_csv('../data/MCI.csv', sep=' ', encoding='utf-8', header=None)

data[1][data[1] > -0.5] = 0

# temp.index = [i for i in range(len(temp))]

plt.rcParams['font.sans-serif'] = ['SimHei']
plt.figure(figsize=(50, 10))
plt.plot(data.index, data, "bp--")
y = [-0.5 for i in range(len(data))]
plt.plot(data.index, y, "r")
plt.show()
