#!/usr/bin/env python
# encoding: utf-8

"""
@version: 0.1
@author: zCoding
@time: 2018/4/26 20:28 Python3
@describe: ....
"""
import os
import warnings
from math import gamma, sqrt

import numpy as np
import pandas as pd
from scipy import integrate

warnings.filterwarnings("ignore")
# 定义全局变量
floatnum = 6  # 决定计算过程中小数点的精度
data = pd.DataFrame()


def getDaysWaterWater(start, length):
    global data
    if start + length < len(data):
        daysWater = data['20-20时降水量/0.1mm'].iloc[start:start + length] / 10.0
    return daysWater


def getDaysTemperature(start, length):
    global data
    if start + length < len(data):
        daysTemperature = data['平均气温/0.1℃'].iloc[start:start + length] / 10.0
    return daysTemperature


def MI(day, length=30):
    global data

    P = getDaysWaterWater(day, length).sum()  # 当前天数之前30天（算了今天，不算今天day+1）的总降水量
    Ti = []
    for i in range(12):
        Ti.append(getDaysTemperature(day + (i * 30), length).sum() / 30)

    if Ti[0] <= 0:
        PET = 0
    else:
        Ti = np.array(Ti)
        for i in range(len(Ti)):
            if Ti[i] <= 0:
                Ti[i] = 0
        H = ((Ti / 5.0) ** 1.514).sum()
        A = 6.75 * (10 ** -7) * H ** 3 - 7.71 * (10 ** -5) * H ** 2 + 1.792 * (10 ** -2) * H + 0.49
        PET = 16 * (10 * Ti[0] / H) ** A
        PET = round(PET, floatnum)
    if PET <= 0:
        MI = P
    else:
        MI = (P - PET) / PET
    MI = round(MI, floatnum)
    return MI


def SPI(day, length):
    # blue = 0
    P = getDaysWaterWater(day, length + 1)  # length(90)天的每天的降水量
    bx = P[day]

    xp = data['20-20时降水量/0.1mm'].mean() / 10  # 降水量气候平均值

    if bx == 0:
        # blue = 1
        m = len(P[P.isin([0])]) - 1  # 0的个数
        n = len(P) - 1  # 所有数据的个数
        Fx = float(m / n)
        Fx = round(Fx, floatnum)
    else:
        tempP = P[~P.isin([0])]  # 去除P中降水量为0的天数
        tempP.pop(day)
        A = np.log10(xp) - tempP.apply(np.log10).sum() / length  # tempP.apply(np.log10) 的意思是TempP取log10
        A = round(A, floatnum)
        r = (1 + sqrt(1 + 0.75 * A)) / (4 * A)
        r = round(r, floatnum)
        x0 = P[1:].sum()
        Beta = xp / r
        try:
            Rr = gamma(r)
        except:
            r = np.log10(r)
            Rr = gamma(r)
        tempBR = 1 / (Rr * Beta ** r)
        AAA = lambda x: tempBR * x ** (r - 1) * np.e ** (-x / Beta)
        Fx = integrate.quad(AAA, 0, x0)[0]
        Fx = round(Fx, floatnum)
    if np.isnan(Fx):
        m = len(P[P.isin([0])]) - 1  # 0的个数
        n = len(P) - 1  # 所有数据的个数
        Fx = float(m / n)
    S = -1
    if Fx == 0:
        Fx = 0.000000001
    if Fx == 1:
        Fx = 0.99
    if Fx > 0.5:
        Fx = 1 - Fx
        S = 1

    c0 = 2.515517
    c1 = 0.802853
    c2 = 0.010328
    d1 = 1.432788
    d2 = 0.189269
    d3 = 0.001308
    tempFx = 1 / Fx ** 2
    templog = np.log(tempFx)
    t = sqrt(templog)
    t = round(t, floatnum)
    SPI = S * (t - (((c2 * t + c1) * t + c0)) / (((d3 * t + d2) * t + d1) * t + 1.0))
    SPI = round(SPI, floatnum)
    return SPI


def SPIW(day, length=60):
    P = getDaysWaterWater(day, length + 1)  # length天的每天的降水量
    WAP = P.sum() * 0.85 ** length

    bx = P[day]

    xp = data['20-20时降水量/0.1mm'].mean() / 10  # 降水量气候平均值
    if bx == 0:
        # blue = 1
        m = len(P[P.isin([0])]) - 1  # 0的个数
        n = len(P) - 1  # 所有数据的个数
        Fx = float(m / n)
        Fx = round(Fx, floatnum)
    else:
        tempP = P[~P.isin([0])]  # 去除P中降水量为0的天数
        tempP.pop(day)
        A = np.log10(xp) - tempP.apply(np.log10).sum() / length  # tempP.apply(np.log10) 的意思是TempP取log10
        A = round(A, floatnum)
        r = (1 + sqrt(1 + 0.75 * A)) / (4 * A)
        r = round(r, floatnum)
        x0 = WAP
        Beta = xp / r
        Beta = round(Beta, floatnum)
        try:
            Rr = gamma(r)
        except:
            r = np.log10(r)
            Rr = gamma(r)
        tempBR = 1 / (Rr * Beta ** r)
        AAA = lambda x: tempBR * x ** (r - 1) * np.e ** (-x / Beta)
        Fx = integrate.quad(AAA, 0, x0)[0]
        Fx = round(Fx, floatnum)
    if np.isnan(Fx):
        m = len(P[P.isin([0])]) - 1  # 0的个数
        n = len(P) - 1  # 所有数据的个数
        Fx = float(m / n)
    S = -1
    if Fx == 0:
        Fx = 0.000000001
    if Fx == 1:
        Fx = 0.99
    if Fx > 0.5:
        Fx = 1 - Fx
        S = 1

    c0 = 2.515517
    c1 = 0.802853
    c2 = 0.010328
    d1 = 1.432788
    d2 = 0.189269
    d3 = 0.001308
    tempFx = 1 / Fx ** 2
    templog = np.log(tempFx)
    t = sqrt(templog)
    t = round(t, floatnum)
    SPIW = S * (t - (((c2 * t + c1) * t + c0)) / (((d3 * t + d2) * t + d1) * t + 1.0))
    SPIW = round(SPIW, floatnum)
    return SPIW


def MCI(j):
    a, b, c, d = 0.3, 0.5, 0.3, 0.2
    k = 1
    if data.iloc[j]['月'] == 3:
        k = 1.2
    if data.iloc[j]['月'] == 4 or data.iloc[j]['月'] == 6 or data.iloc[j]['月'] == 7 or data.iloc[j]['月'] == 8:
        k = 1.1
    MCI = a * SPIW(j) + b * MI(j) + c * SPI(j, 90) + d * SPI(j, 150)
    MCI = MCI * k
    return MCI


def main():
    global data
    file_dir = 'D:/zCoding/DaQi/data/ordata'
    for root, dirs, files in os.walk(file_dir):
        print("\n当前目录路径", root)  # 当前目录路径
        print("\n当前路径下所有非目录子文件", files)  # 当前路径下所有非目录子文件

    for fileName in files:
        path = root + "/" + fileName
        print(path)
        data = pd.read_csv(path, encoding='GB2312')

        result = data
        # result['MCI'] = 0
        for day in range(0, len(data) - 361):
            try:
                # result = result.append({'day': day, 'MCI': MCI(day)}, ignore_index=True)
                result.loc[day, 'MCI'] = MCI(day)
            except Exception as err:
                print("有异常抛出", format(err))
                upday = result.loc[day - 1, 'MCI']
                result.loc[day, 'MCI'] = upday - 0.45
                # result = result.append({'day': day, 'MCI': upday}, ignore_index=True)
                continue
        result.to_csv('../data/mcidata/MCI' + fileName, index=False, encoding='GB2312')
    print("finsh")


main()
